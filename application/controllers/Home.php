<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('home');
	}

	public function open_billboard()
	{
		$this->load->view('open_billboard');
	}
	public function closed_billboard()
	{
		$this->load->view('closed_billboard');
	}
}
