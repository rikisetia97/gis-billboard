<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Api_Model', 'Api');
    }

    public function auth()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $role = $this->input->post('role');
        $data = $this->Api->login($username, $password, $role);
        echo $data;
    }
    public function create_user()
    {
        $fullname = $this->input->post('fullname');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $data = $this->Api->create_user($username, $password, $fullname);
        echo $data;
    }

    public function logout()
    {
        $this->session->sess_destroy('sess_auth');
        return true;
    }

    public function add_billboard()
    {
        $data = $this->input->post();
        $config['upload_path']      = './uploads/';
        $config['allowed_types']    = 'jpg|jpeg|png';
        $config['remove_spaces']    = TRUE;
        $config['encrypt_name']     = TRUE;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('file')) {
            $data['image'] = base_url('uploads/' . $this->upload->data('file_name'));
            $this->Api->add_billboard($data);
            echo true;
        } else {
            echo false;
        }
    }

    public function get_all_billboard()
    {
        $data = $this->Api->get_all_billboard();
        echo $data;
    }
    public function get_data_billboard()
    {
        $search = $this->input->get('search');
        $data = $this->Api->get_data_billboard($search);
        echo $data;
    }
    public function get_open_billboard()
    {
        $search = $this->input->get('search');
        $data = $this->Api->get_billboard(1, $search);
        echo $data;
    }
    public function get_closed_billboard()
    {
        $search = $this->input->get('search');
        $data = $this->Api->get_billboard(0, $search);
        echo $data;
    }
    public function delete_data_billboard($id)
    {
        $data = $this->Api->delete_data_billboard($id);
        echo $data;
    }
}
