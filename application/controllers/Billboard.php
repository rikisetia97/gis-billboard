<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Billboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function daftar_billboard()
    {
        $this->load->view('admin/billboard/daftar_billboard');
    }
    public function add_billboard()
    {
        $this->load->view('admin/billboard/add_billboard');
    }
    public function pemesanan_billboard()
    {
        $this->load->view('admin/billboard/pemesanan_billboard');
    }
    public function about()
    {
        $this->load->view('about');
    }
    public function contact()
    {
        $this->load->view('contact');
    }
}
