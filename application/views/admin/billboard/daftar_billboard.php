<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <div class="content_billboard">
        <!--tooltip-->
        <div class="jumbotron header" style="margin-top: 0px; position: fixed; background: #fff; z-index: 10; width: 740px;">
            <div class="container">
                <h2><i class="icon-settingstwo-gearalt"></i> Kelola Billboard</h2>
                <p><a class="btn btn-danger btn-sm" id="tambah_baru"><i class="fa fa-plus"></i> Tambah Baru</a></p>
            </div>
        </div>
        <!--/tooltip-->
        <div class="row">
            <div class="col-md-12" style="margin-top: 100px;">
                <div class="input-group">
                    <input type="text" class="form-control search_text" placeholder="Cari billboard...">
                    <span class="input-group-btn">
                        <button class="btn btn-danger search_button" type="button">Cari!</button>
                    </span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row list_data_billboard">
                </div>
            </div>
            <!-- <div class="col-md-12 text-center" style="margin-top: -20px;">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div> -->
        </div>
        <!--/places-->
    </div>
    <script>
        $('#tambah_baru').click(function() {
            getLayout('billboard/add_billboard', $('.content_billboard'))
        })
        $('.search_button').click(function() {

            get_data_billboard($('.search_text').val())
        })
        get_data_billboard()

        function get_data_billboard(search = '') {
            $.ajax({
                type: "GET",
                data: {
                    search
                },
                url: "api/get_data_billboard",
                success: function(response) {
                    $('.list_data_billboard').html('')
                    var obj = JSON.parse(response)
                    obj.forEach(item => {
                        $('.list_data_billboard').append(` <div class="col-sm-6 col-md-4 places">
                        <div class="thumbnail">
                            <img src="` + item.image + `" style="height: 150px; width: 100%; cursor:pointer" alt="..." class="detail" data-lat="` + item.lat + `" data-lng="` + item.long + `"/>
                            <div class="caption">
                                <a href="javascript:void(0)" class="detail" data-lat="` + item.lat + `" data-lng="` + item.long + `">` + item.title + `</a>
                                <p>` + item.description.substring(0, 33) + ` ...</p>
                                <p>
                                    <img src="img/icon/` + (item.status == 1 ? '09' : '08') + `.png" style="height: 20px" alt="..." /> ` + (item.status == 1 ? 'Tersedia' : 'Sedang disewa') + `
                                    <a href="javascript:void(0)"  onclick="delete_billboard(` + item.id + `)" class="btn btn-default btn-xs ping"><i class="icon-trash"></i></a>
                                    <a href="javascript:void(0)" class="btn btn-default btn-xs"><i class="icon-edit"></i></a>
                                </p>` +
                            (item.status == 1 ? `
                                <button class="btn btn-danger btn-block" >Pesan Sekarang</button>
                                ` : `
                                <button class="btn btn-danger btn-block" disabled>Tersedia : 2021-11-20</button>
                                `) +
                            `</div>
                        </div>
                    </div> `)
                    });
                    $(".detail").click(function() {
                        $('.gradientmenu').removeClass('active')
                        $('#cont').attr('style', 'display:none')
                        map.setCenter(new google.maps.LatLng(this.dataset.lat, this.dataset.lng));
                        map.setZoom(17);
                    });
                }
            });
        }

        function delete_billboard(id) {
            $.confirm({
                title: 'Apakah anda yakin?',
                content: 'Anda akan menghapus billboard ini!',
                theme: 'supervan', // 'material', 'bootstrap'
                animation: 'rotateXR',
                closeAnimation: 'rotateX',
                animationSpeed: 500,
                buttons: {
                    'Hapus': {
                        text: 'Hapus',
                        btnClass: 'btn-blue',
                        action: function() {
                            $.ajax({
                                type: "GET",
                                url: "api/delete_data_billboard/" + id,
                                success: function(response) {
                                    get_data_billboard()
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Batal',
                        btnClass: 'btn-blue',
                        action: function() {

                        }
                    }
                }
            });
        }
    </script>
</body>

</html>