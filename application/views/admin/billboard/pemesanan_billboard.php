<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <div class="content_billboard">
        <!--tooltip-->
        <div class="jumbotron header" style="margin-top: 0px; position: fixed; background: #fff; z-index: 10; width: 740px;">
            <div class="container">
                <h2><i class="icon-time"></i> Pemesanan Billboard</h2>
            </div>
        </div>
        <div class="container">
            <div class="form-group row" style="margin-top: 100px;">
                <div class="col-md-8">
                    <label for="exampleInputEmail1">Kode Billboard <span class="text-danger">*</span></label>
                    <select class="form-control kode_billboard">
                        <option value=""></option>
                    </select>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-primary btn-sm btn-block detail" disabled style="margin-top: 20px;"><i class="icon-checkin"></i> Cek Lokasi</button>
                </div>

            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Latitude<span class="text-danger">*</span></label>
                    <input type="text" class="form-control lat" readonly>
                </div>
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Longitude<span class="text-danger">*</span></label>
                    <input type="text" class="form-control long" readonly>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="exampleInputPassword1">NPWP<span class="text-danger">*</span></label>
                    <input type="text" class="form-control npwp">
                </div>
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Nama WP<span class="text-danger">*</span></label>
                    <input type="text" class="form-control nama_wp">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Alamat<span class="text-danger">*</span></label>
                    <textarea class="form-control" cols="10" rows="3"></textarea>
                </div>
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Isi Reklame<span class="text-danger">*</span></label>
                    <textarea class="form-control" cols="10" rows="3"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Tgl. Pemasangan<span class="text-danger">*</span></label>
                    <input type="date" class="form-control tgl_pasang">
                </div>
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Tgl. Akhir Pemasangan<span class="text-danger">*</span></label>
                    <input type="date" class="form-control tgl_akhir_pasang">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Lama Pemasangan<span class="text-danger">*</span></label>
                    <input type="number" class="form-control lama_pasang">
                </div>
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Harga Sewa<span class="text-danger">*</span></label>
                    <input type="number" class="form-control harga_sewa" readonly>
                </div>
            </div>
            <button class="btn btn-danger btn-block"><i class="icon-save-floppy"></i> Simpan</button>
        </div>
    </div>
    <script>
        $('.kode_billboard').select2({
            placeholder: "Pilih Kode Billboard"
        });
        $('.kode_billboard').html('')
        $.ajax({
            type: "GET",
            url: "api/get_open_billboard",
            success: function(response) {
                var obj = JSON.parse(response)
                $('.kode_billboard').append('<option value=""></option>')
                obj.forEach(item => {
                    $('.kode_billboard').append('<option data-lat="' + item.lat + '" data-long="' + item.long + '" value="' + item.kode_billboard + '">' + item.kode_billboard + ' | ' + item.title + '</option>')
                });
                if (localStorage.getItem('tmp')) {
                    var obj = JSON.parse(localStorage.getItem('tmp'))
                    $(".kode_billboard").val(obj.kode_billboard).trigger('change');
                }
            }
        });

        $('.kode_billboard').change(function() {
            $('.detail').removeAttr('disabled')
            var lat = $(this).find(':selected').data('lat')
            var long = $(this).find(':selected').data('long')
            $('.lat').val(lat)
            $('.long').val(long)
        })

        $(".detail").click(function() {
            $('.gradientmenu').removeClass('active')
            $('#cont').attr('style', 'display:none')
            map.setCenter(new google.maps.LatLng($('.lat').val(), $('.long').val()));
            map.setZoom(17);
            var data_tmp = {
                kode_billboard: $('.kode_billboard').val()
            }
            localStorage.setItem('tmp', JSON.stringify(data_tmp))
        });
    </script>
</body>

</html>