<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <div class="content_billboard">
        <!--tooltip-->
        <div class="jumbotron header" style="margin-top: 0px; position: fixed; background: #fff; z-index: 10; width: 740px;">
            <div class="container">
                <h2>Tambah Billboard</h2>
                <p><a class="btn btn-success btn-sm btn-save-billboard" id="simpan"><i class="fa fa-save"></i> Simpan</a></p>
                <p><a class="btn btn-default btn-sm" style="margin-right: 10px;" id="kembali"><i class="fa fa-arrow-left"></i> Kembali</a></p>
            </div>
        </div>
        <div class="container">
            <div class="form-group" style="margin-top: 100px;">
                <label for="exampleInputEmail1">Kode Billboard <span class="text-danger">*</span></label>
                <input type="text" class="form-control kode_billboard" placeholder="Kode Billboard">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Nama Billboard <span class="text-danger">*</span></label>
                <input type="text" class="form-control title" placeholder="Nama Billboard">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Harga Sewa Per Tahun <span class="text-danger">*</span></label>
                <input type="number" class="form-control harga" placeholder="Harga Sewa">
            </div>
            <div id="map_canvas_add" style="width: 100%; height: 250px; margin-bottom: 15px;"> </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Latitude<span class="text-danger">*</span></label>
                    <input type="text" class="form-control lat" readonly>
                </div>
                <div class="col-md-6">
                    <label for="exampleInputPassword1">Longitude<span class="text-danger">*</span></label>
                    <input type="text" class="form-control long" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword2">Deskripsi</label>
                <textarea name="" class="form-control description" cols="5" rows="5" placeholder="Masukkan keterangan billboard disini"></textarea>
            </div>
            <div class="form-group" style="margin-bottom: 50px;">
                <label for="formFile">Upload Gambar<span class="text-danger">*</span></label>
                <input class="form-control" type="file" id="formFile">
            </div>

        </div>
    </div>
    <script>
        $('.btn-save-billboard').click(function() {
            var myform = new FormData()
            myform.append('file', $('#formFile')[0].files[0])
            myform.append('kode_billboard', $('.kode_billboard').val())
            myform.append('title', $('.title').val())
            myform.append('lat', $('.lat').val())
            myform.append('long', $('.long').val())
            myform.append('description', $('.description').val())
            $.ajax({
                type: "POST",
                url: "api/add_billboard",
                data: myform,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response) {
                        location.reload()
                    } else {
                        alert('gagal menambahkan billboard!')
                    }
                }
            });
        })
        $('#kembali').click(function() {
            getLayout('billboard/daftar_billboard', $('#content-tab'))
        })
        initMap()


        function initMap() {
            var map2 = new google.maps.Map(document.getElementById("map_canvas_add"), {
                zoom: 10,
                center: new google.maps.LatLng(-6.955799, 109.698785),
                mapId: "82c92d1916a6e129",
                // mapId: "86a32e5063daa233",
                mapTypeId: 'terrain',
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.RIGHT_BOTTOM,
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_CENTER,
                },
                zoomControl: true,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_CENTER,
                },
            });
            var marker = new google.maps.Marker({
                position: {
                    lat: 0,
                    lng: 0
                }
            });

            google.maps.event.addListener(map2, 'click', function(e) {
                console.log(e)
                placeMarker(e.latLng, map2, marker);
            });

            function placeMarker(latlng, map, marker) {
                marker.setPosition(latlng);
                marker.setMap(map2);
                map.panTo(latlng);
                // lat = latlng.replace('(', '').replace(')', '').split(',')
                $('.lat').val(latlng)
                lat = $('.lat').val().replace('(', '').replace(')', '').split(',')
                $('.lat').val(lat[0])
                $('.long').val(lat[1])
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        }
    </script>
</body>

</html>