<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <div class="content_billboard">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-success"><i class="icon-newtab"></i> Daftar Tersedia</h4>
            </div>
            <div class="col-md-12" style="margin-top: 10px;">
                <div class="input-group">
                    <input type="text" class="form-control search_text" placeholder="Search billboard...">
                    <span class="input-group-btn">
                        <button class="btn btn-success search_btn" type="button">Cari!</button>
                    </span>
                </div>
            </div>

            <div class="col-md-12">
                <div class="row list_data_billboard">
                </div>
            </div>
            <!-- <div class="col-md-12 text-center" style="margin-top: -20px;">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div> -->
        </div>
        <!--/places-->
    </div>
    <script>
        $('#tambah_baru').click(function() {
            getLayout('billboard/add_billboard', $('.content_billboard'))
        })
        get_data_billboard()

        $('.search_btn').click(function() {
            get_data_billboard($('.search_text').val())
        })

        function get_data_billboard(search = '') {
            $.ajax({
                type: "GET",
                data: {
                    search: search
                },
                url: "api/get_open_billboard",
                success: function(response) {
                    $('.list_data_billboard').html('')
                    var obj = JSON.parse(response)
                    obj.forEach(item => {
                        $('.list_data_billboard').append(` <div class="col-sm-6 col-md-4 places">
                        <div class="thumbnail" >
                            <img src="` + item.image + `" style="height: 150px; width: 100%; cursor:pointer" alt="..." class="detail" data-lat="` + item.lat + `" data-lng="` + item.long + `"/>
                            <div class="caption">
                                <a href="javascript:void(0)"  class="detail text-success" data-lat="` + item.lat + `" data-lng="` + item.long + `" style="cursor:pointer">` + item.title + `</a>
                                <p>` + item.description.substring(0, 33) + ` ...</p>
                                <p>
                                    <img src="img/icon/` + (item.status == 1 ? '09' : '08') + `.png" style="height: 20px" alt="..." /> ` + (item.status == 1 ? 'Tersedia' : 'Sedang disewa') + `
                                    <a href="javascript:void(0)" class="btn btn-default btn-xs"><i class="icon-map-marker"></i></a>
                                </p>
                            </div>
                        </div>
                    </div> `)
                    });
                    $(".detail").click(function() {
                        $('.gradientmenu').removeClass('active')
                        $('#cont').attr('style', 'display:none')
                        map.setCenter(new google.maps.LatLng(this.dataset.lat, this.dataset.lng));
                        map.setZoom(17);
                    });
                }
            });
        }

        function delete_billboard(id) {
            $.confirm({
                title: 'Apakah anda yakin?',
                content: 'Anda akan menghapus billboard ini!',
                theme: 'supervan', // 'material', 'bootstrap'
                buttons: {
                    "Hapus": function() {
                        $.ajax({
                            type: "GET",
                            url: "api/delete_data_billboard/" + id,
                            success: function(response) {
                                get_data_billboard()
                            }
                        });
                    },
                    "Batal": function() {}
                }
            });
        }
    </script>
</body>

</html>