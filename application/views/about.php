<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <div class="content_billboard">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-danger"><i class="icon-info-sign"></i> Tentang Aplikasi</h4>
            </div>
            <div class="col-md-12 youcan">
                <ul>
                    <li><span>1</span> Aplikasi billboard bertujuan untuk membantu dalam pengelolaan billboard maupun pemesanan billboard, serta calon-calon penyewa juga dapat melihat dan memantau secara langsung lokasi billboard yang akan disewanya.<br></li>
                </ul>
            </div>

        </div>
    </div>
</body>

</html>