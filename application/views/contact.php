<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <div class="content_billboard">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-danger"><i class="icon-importcontacts"></i> Hubungi Kami</h4>
            </div>
            <div class="col-md-12 tweets">
                <span style="color: white;">
                    Jika anda ada pertanyaan terkait ketersediaan billboard atau hal lain dapat hubungi kami. atau isi form dibawah untuk bertanya.
                </span>
            </div>
            <div class="col-md-6">
                <button class="btn btn-success btn-block"><i class="icon-callalt"></i> Hubungi via Whatsapp</button>
            </div>
            <div class="col-md-6">
                <button class="btn btn-primary btn-block"><i class="icon-emailalt"></i> Hubungi via Email</button>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <label for="exampleInputEmail1">Nama Lengkap <span class="text-danger">*</span></label>
            <input type="text" class="form-control" placeholder="Nama Lengkap">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Alamat Email <span class="text-danger">*</span></label>
            <input type="email" class="form-control" placeholder="Alamat Email Anda">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Subjek <span class="text-danger">*</span></label>
            <input type="text" class="form-control" placeholder="Topik Pesan Anda">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Isi Pesan <span class="text-danger">*</span></label>
            <textarea class="form-control" cols="7" rows="10"></textarea>
        </div>
        <button class="btn btn-primary btn-block"><i class="icon-emailalt"></i> Kirim Pesan</button>

    </div>
</body>

</html>