<!DOCTYPE html>
<html>

<head>
    <title>Bill's Geometry</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?= base_url(); ?>css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="<?= base_url(); ?>css/main.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="<?= base_url(); ?>css/whhg.css">
    <link rel="stylesheet" href="<?= base_url(); ?>css/wenk.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url(); ?>js/bootstrap.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/owl.carousel.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsbzuJDUEOoq-jS1HO-LUXW4qo0gW9FNs&callback=initMap&v=beta" async></script>
    <script type="text/javascript" src="<?= base_url(); ?>js/map.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <style>
        .ui-button {
            background-color: #fff;
            border: 0;
            border-radius: 2px;
            box-shadow: 0 1px 4px -1px rgba(0, 0, 0, 0.3);
            margin-right: 10px;
            font: 400 18px Roboto, Arial, sans-serif;
            overflow: hidden;
            height: 40px;
            cursor: pointer;
        }

        .ui-button:hover {
            background: #ebebeb;
        }
    </style>
</head>

<body onload="initialize()">
    <!--map-->
    <div id="map_canvas" class="map"></div>
    <div id="overview"></div>
    <div id="legend" style="background-color: white; padding: 3px; margin: 10px; color: #666666; border-radius: 3px;">
        <span style="padding: 20px; margin-bottom: 100px;">
            <a href="javascript:void(0)">INFORMASI PETA</a>
        </span>
    </div>
    <!--/map-->
    <div class="row site">
        <div class="col-md-1 general_menu inner">
            <ul>
                <?php if (!$this->session->userdata('sess_auth')) { ?>
                    <li style="margin-bottom: 5px; margin-top: 5px;">
                        <button class="btn btn-success btn-block btn-lg auth_confirm" data-wenk="Login" data-wenk-pos="right"><i class="icon-enter"></i></button>
                    </li>
                <?php } ?>
                <li style="margin-bottom: 5px;margin-top: 5px;">
                    <button class="btn btn-white btn-block btn-lg btn-refresh" data-wenk="Refresh Halaman" data-wenk-pos="right"><i class="icon-refresh"></i></button>
                </li>
                <li>
                    <a href="#" class="lg-sidebar gradientmenu" data-tab="home/open_billboard" data-wenk="Billboard Tersedia" data-wenk-pos="right"><i class="icon-newtab"></i></a>
                </li>
                <li>
                    <a href="#" class="lg-sidebar gradientmenu" data-tab="home/closed_billboard" data-wenk="Billboard Sedang Disewa" data-wenk-pos="right"><i class="icon-closetab"></i></a>
                </li>
                <?php if (@$this->session->userdata('sess_auth')->role == 'admin') { ?>
                    <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="billboard/daftar_billboard" data-wenk="Kelola Billboard" data-wenk-pos="right"><i class="icon-storage-box"></i></a>
                    </li>
                    <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="" data-wenk="Daftar Pesanan" data-wenk-pos="right"><i class="icon-indexmanager"></i></a>
                    </li>
                    <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="" data-wenk="Daftar Penyewa" data-wenk-pos="right"><i class="icon-user"></i></a>
                    </li>
                    <!-- <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="billboard/pemesanan_billboard" data-wenk="Pemesanan" data-wenk-pos="right"><i class="icon-time"></i></a>
                    </li> -->
                    <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="" data-wenk="Laporan" data-wenk-pos="right"><i class="icon-report"></i></a>
                    </li>

                    <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="" data-wenk="Kelola Administrator" data-wenk-pos="right"><i class="icon-userfilter"></i></a>
                    </li>
                    <!-- <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="billboard/contact" data-wenk="Hubungi Kami" data-wenk-pos="right"><i class="icon-importcontacts"></i></a>
                    </li> -->
                    <!-- <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="billboard/about" data-wenk="Tentang Aplikasi" data-wenk-pos="right"><i class="icon-info-sign"></i></a>
                    </li> -->
                    <li style="margin-bottom: 5px;margin-top: 5px;">
                        <button class="btn btn-danger btn-block btn-lg logout_confirm" data-wenk="Logout" data-wenk-pos="right"><i class="icon-exit"></i></button>
                    </li>
                <?php } else if (@$this->session->userdata('sess_auth')->role == 'user') { ?>
                    <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="billboard/pemesanan_billboard" data-wenk="Pemesanan" data-wenk-pos="right"><i class="icon-time"></i></a>
                    </li>
                    <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="billboard/contact" data-wenk="Hubungi Kami" data-wenk-pos="right"><i class="icon-importcontacts"></i></a>
                    </li>
                    <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="billboard/about" data-wenk="Tentang Aplikasi" data-wenk-pos="right"><i class="icon-info-sign"></i></a>
                    </li>
                    <li style="margin-bottom: 5px;margin-top: 5px;">
                        <button class="btn btn-danger btn-block btn-lg logout_confirm" data-wenk="Logout" data-wenk-pos="right"><i class="icon-exit"></i></button>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="billboard/contact" data-wenk="Hubungi Kami" data-wenk-pos="right"><i class="icon-importcontacts"></i></a>
                    </li>
                    <li>
                        <a href="#" class="lg-sidebar gradientmenu" data-tab="billboard/about" data-wenk="Tentang Aplikasi" data-wenk-pos="right"><i class="icon-info-sign"></i></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <div class="col-md-11 side-bar" id="cont">
            <div id="content-tab">
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var jc = $.confirm({
            theme: 'supervan', // 'material', 'bootstrap'
            animation: 'rotateXR',
            closeAnimation: 'rotateX',
            animationSpeed: 500
        });
        setTimeout(() => {
            jc.showLoading()
        }, 10);
        setTimeout(() => {
            jc.close()
        }, 1000);
        $('.auth_confirm').click(function() {
            $.confirm({
                title: 'Login ke sistem!',
                theme: 'supervan', // 'material', 'bootstrap'
                content: '' +
                    '<form class="formName">' +
                    '<div class="form-group">' +
                    '<label>Username</label>' +
                    '<input type="text" placeholder="Your username" class="name form-control" required />' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label>Password</label>' +
                    '<input type="password" placeholder="Your password" class="password form-control" required />' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label>Masuk sebagai</label>' +
                    '<select class="form-control role"><option value="user">User (Pemesan)</option><option value="admin">Administrator</option></select>' +
                    '</div>' +
                    '</form>' +
                    '<a href="javascript:void(0)" onclick="register_modal()">Daftar User (Pemesan) Baru</a>',
                buttons: {
                    formSubmit: {
                        text: 'Login',
                        btnClass: 'btn-blue',
                        action: function() {
                            var name = this.$content.find('.name').val();
                            var password = this.$content.find('.password').val();
                            var role = this.$content.find('.role').val();
                            $.ajax({
                                type: "POST",
                                url: "api/auth",
                                data: {
                                    username: name,
                                    password: password,
                                    role: role,
                                },
                                cache: false,
                                success: function(response) {
                                    if (response) {
                                        location.reload()
                                    } else {
                                        $.alert('Username atau password salah');
                                    }
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Batal',
                        btnClass: 'btn-blue',
                        action: function() {}
                    }
                },
            });
        })

        function register_modal() {
            $.confirm({
                title: 'Daftar User (Pemesan) Baru',
                theme: 'material', // 'material', 'bootstrap'
                content: '' +
                    '<form class="formName">' +
                    '<div class="form-group">' +
                    '<label>Nama Lengkap</label>' +
                    '<input type="text" placeholder="Your fullname" class="_fullname form-control" required />' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label>Nomor HP</label>' +
                    '<input type="number" placeholder="Your phone number" class="_no_hp form-control" required />' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label>Username</label>' +
                    '<input type="text" placeholder="Your username" class="_username form-control" required />' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label>Password</label>' +
                    '<input type="password" placeholder="Your password" class="_password form-control" required />' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label>Confirm Password</label>' +
                    '<input type="password" placeholder="Repeat password" class="_re_password form-control"  required />' +
                    '</div>' +
                    '</form>',
                buttons: {
                    formSubmit: {
                        text: 'Daftar Sekarang',
                        btnClass: 'btn-danger',
                        action: function() {
                            var fullname = this.$content.find('._fullname').val();
                            var username = this.$content.find('._username').val();
                            var password = this.$content.find('._password').val();
                            var re_password = this.$content.find('._re_password').val();
                            var no_hp = this.$content.find('.no_hp').val();
                            console.log()
                            $.ajax({
                                type: "POST",
                                url: "api/create_user",
                                data: {
                                    fullname: fullname,
                                    username: username,
                                    password: password,
                                    no_hp: no_hp,
                                },
                                cache: false,
                                success: function(response) {
                                    if (response) {
                                        location.reload()
                                    } else {
                                        $.alert('Username atau password salah');
                                    }
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Batal',
                        btnClass: 'btn-blue',
                        action: function() {}
                    }
                },
            });
        }
        $('.logout_confirm').click(function() {
            $.confirm({
                title: 'Apakah anda yakin?',
                content: 'Anda akan logout dari aplikasi ini!',
                theme: 'supervan', // 'material', 'bootstrap'
                buttons: {
                    'Logout': {
                        text: 'Logout',
                        btnClass: 'btn-blue',
                        action: function() {
                            $.ajax({
                                type: "GET",
                                url: "api/logout",
                                success: function(response) {
                                    location.reload()
                                }
                            });
                        }
                    },
                    cancel: {
                        text: 'Batal',
                        btnClass: 'btn-blue',
                        action: function() {

                        }
                    }
                }
            });
        })
        $('.general_menu').hide()

        function getLayout(url = '', div) {
            div.html('')
            if (url != '') {
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(response) {
                        div.html(response)
                    }
                });
            }
        }
        $(document).ready(function() {
            $('.gradientmenu').removeClass('active')
            $('#cont').attr('style', 'display:none')
            setTimeout(() => {
                $('.general_menu').slideUp().fadeIn('fast');
            }, 350);
            $(".inner ul li a").click(function() {
                if ($(this).hasClass('active')) {
                    $('.gradientmenu').removeClass('active')
                    $('#cont').attr('style', 'display:none')
                } else {
                    $('#cont').removeAttr('style')
                    if ($(this).hasClass('lg-sidebar')) {
                        $('#cont').attr('class', 'col-md-11 general_content_styles index_content')
                    } else {
                        $('#cont').attr('class', 'col-md-11 side-bar')
                    }
                    $(".inner ul li a").removeClass("active");
                    $("#tabs .active").removeClass("active");
                    $(this).addClass("active");
                    getLayout(this.dataset.tab, $('#content-tab'))
                }

                return false;
            })
            $("#tabs_point li a").each(function(i) {
                $("#tabs_point li a:eq(" + i + ")").click(function() {
                    var tab_id = i + 1;
                    $("#tabs_point li a").removeClass("active");
                    $(".tabs_block_point .active").removeClass("active");
                    $(this).addClass("active");
                    $(".tabs_block_point div").stop(false, false).hide();
                    $("#point_tab" + tab_id).stop(false, false).show();
                    return false;
                })
            })
            $('#link_open').on('click', function() {
                if ($('#link_open').hasClass("clooses")) {
                    $("#open_span").removeClass("close_span").addClass("open_span");
                    $("#profile").removeClass("profile_closed");
                    $("#link_open").removeClass("clooses");
                    $("#cont").addClass("none");
                } else {
                    $("#open_span").addClass("close_span").removeClass("open_span");
                    $("#profile").addClass("profile_closed");
                    $("#link_open").addClass("clooses");
                    $("#cont").removeClass("none");
                }
            })
            $('#map_open').on('click', function() {
                "use strict";
                $("#cont").addClass("none");
                $("#Show_cont").removeClass("none");

            })
            $('#Show_cont').on('click', function() {
                "use strict";
                $("#cont").removeClass("none");
            })
            $('#map_canvas').click(function() {
                $('.gradientmenu').removeClass('active')
                $('#cont').attr('style', 'display:none')
            })
        });

        $('.btn-refresh').click(function() {
            location.reload()
        })
    </script>
    <script type="text/javascript">
        $(function() {
            "use strict";
            $("#owl-demo, #myguest").owlCarousel({
                items: 6,
                itemsDesktop: [1000, 5],
                itemsDesktopSmall: [900, 6],
                itemsTablet: [600, 2],
                itemsMobile: false
            });

        });
    </script>
    <!--/Script for worked profile page-->
</body>

</html>