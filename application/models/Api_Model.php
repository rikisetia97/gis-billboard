<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Api_Model extends CI_Model
{

    public function create_user($username, $password, $fullname)
    {
        print_r(password_hash($password, PASSWORD_DEFAULT));
        die;
        // $this->db->get_where('tm_admin');
    }
    public function login($username, $password, $role)
    {
        if ($role == 'admin') {
            $table = 'tm_admin';
        } else {
            $table = 'tm_user';
        }
        $check = $this->db->get_where($table, ['username' => $username])->row();
        if ($check) {
            $check_pass = password_verify($password, $check->password);
            if ($check_pass) {
                unset($check->password);
                $check->role = $role;
                $this->session->set_userdata('sess_auth', $check);
                return true;
            }
        }


        return false;
    }
    public function get_all_billboard()
    {
        $this->db->select('title,lat,long,description,image,status,kode_billboard');
        $query = $this->db->get('tm_billboard')->result();
        $result = [];
        foreach ($query as $item) {
            $arr = [
                $item->title,
                $item->lat,
                $item->long,
                $item->description,
                $item->image,
                $item->status,
                $item->kode_billboard,
            ];
            $result[] = $arr;
        }
        return json_encode($result);
    }
    public function get_data_billboard($search)
    {
        if ($search) {
            $this->db->like('title', $search);
        }
        $query = $this->db->get('tm_billboard')->result();
        return json_encode($query);
    }
    public function get_billboard($status, $search)
    {
        $this->db->where('status', $status);
        if ($search) {
            $this->db->like('title', $search);
        }
        $query = $this->db->get('tm_billboard')->result();
        return json_encode($query);
    }
    public function delete_data_billboard($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tm_billboard');
        return true;
    }

    public function add_billboard($data)
    {
        $this->db->insert('tm_billboard', $data);
        return true;
    }
}
