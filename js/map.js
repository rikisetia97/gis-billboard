
var locations = [], locations_booked = []
var toggle_map = false
function initialize() {
    locations = [], locations_booked = []
    $.ajax({
        type: "GET",
        url: "api/get_all_billboard",
        success: function (response) {
            response = JSON.parse(response)
            response.forEach(item => {
                if (item[5] == 1) {
                    locations.push(item)
                } else {
                    locations_booked.push(item)
                }
            });
            init_map()
        }
    });

}
var map
const jawa = { lat: -6.955799, lng: 109.698785 };
function CenterControl(controlDiv, map) {
    // Set CSS for the control border.
    const controlUI = document.createElement("div");

    controlUI.style.backgroundColor = "#fff";
    controlUI.style.border = "2px solid #fff";
    controlUI.style.borderRadius = "3px";
    controlUI.style.boxShadow = "0 2px 6px rgba(0,0,0,.3)";
    controlUI.style.cursor = "pointer";
    controlUI.style.marginTop = "8px";
    controlUI.style.marginBottom = "22px";
    controlUI.style.textAlign = "center";
    controlUI.title = "Click to recenter the map";
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    const controlText = document.createElement("div");

    controlText.style.color = "rgb(25,25,25)";
    controlText.style.fontFamily = "Roboto,Arial,sans-serif";
    controlText.style.fontSize = "16px";
    controlText.style.lineHeight = "38px";
    controlText.style.paddingLeft = "5px";
    controlText.style.paddingRight = "5px";
    controlText.innerHTML = "◉ Default View Map";
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to jawa.
    controlUI.addEventListener("click", () => {
        map.setCenter(jawa);
        map.setZoom(10);
    });
}
function init_map() {
    map = new google.maps.Map(document.getElementById("map_canvas"), {
        zoom: 10,
        center: new google.maps.LatLng(-6.955799, 109.698785),
        mapId: "82c92d1916a6e129",
        // mapId: "86a32e5063daa233",
        mapTypeId: 'terrain',
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.RIGHT_BOTTOM,
        },
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER,
        },
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER,
        },
    });
    const buttons = [
        ["⏪", "rotate", 20, google.maps.ControlPosition.RIGHT_CENTER],
        ["⏩", "rotate", -20, google.maps.ControlPosition.RIGHT_CENTER],
        ["⏫", "tilt", 20, google.maps.ControlPosition.RIGHT_CENTER],
        ["⏬", "tilt", -20, google.maps.ControlPosition.RIGHT_CENTER],
    ];

    buttons.forEach(([text, mode, amount, position]) => {
        const controlDiv = document.createElement("div");
        const controlUI = document.createElement("button");

        controlUI.classList.add("ui-button");
        controlUI.innerText = `${text}`;
        controlUI.addEventListener("click", () => {
            adjustMap(mode, amount);
        });
        controlDiv.appendChild(controlUI);
        map.controls[position].push(controlDiv);
    });

    const adjustMap = function (mode, amount) {
        switch (mode) {
            case "tilt":
                map.setTilt(map.getTilt() + amount);
                break;
            case "rotate":
                map.setHeading(map.getHeading() + amount);
                break;
            default:
                break;
        }
    };
    // constructor passing in this DIV.
    const centerControlDiv = document.createElement("div");

    CenterControl(centerControlDiv, map);

    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);


    const icons = {
        parking: {
            name: " Sedang disewa (" + locations_booked.length + ")",
            icon: 'img/icon/08.png',
        },
        library: {
            name: " Tersedia (" + locations.length + ")",
            icon: 'img/icon/09.png',
        },
    };
    const legend = document.getElementById("legend");
    if (!toggle_map) {
        for (const key in icons) {
            const type = icons[key];
            const name = type.name;
            const icon = type.icon;
            const div = document.createElement("div");

            div.innerHTML = '<img class="img-thumbnail" width="25px" style="margin-right:5px; margin-top:5px" src="' + icon + '"> ' + name;
            legend.appendChild(div);
        }
        toggle_map = true
    }
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
            icon: {
                url: 'img/icon/09.png',
                labelOrigin: { x: 56, y: 12 }
            },
            animation: google.maps.Animation.DROP,
            label: {
                text: locations[i][6],
                color: '#666666',
            }
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                if (marker.getAnimation() !== null) {
                    marker.setAnimation(null);
                } else {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                }
                map.setZoom(17);
                myLatLng = new google.maps.LatLng(locations[i][1], locations[i][2]);
                marker.setPosition(myLatLng);
                map.panTo(myLatLng);
            }
        })(marker, i));
        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
            return function () {
                infowindow.setContent("<div><img src='" + locations[i][4] + "' width='200px' height='130px'></div><h5>" + locations[i][0] + "</h5><span>" + locations[i][3] + "</span>");
                infowindow.open(map, marker);
            }
        })(marker, i));
        google.maps.event.addListener(marker, 'mouseout', (function (marker, i) {
            return function () {
                marker.setAnimation(null);
                infowindow.close();
            }
        })(marker, i));
    }
    for (i = 0; i < locations_booked.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations_booked[i][1], locations_booked[i][2]),
            map: map,
            icon: {
                url: 'img/icon/08.png',
                labelOrigin: { x: 56, y: 12 }
            },
            animation: google.maps.Animation.DROP,
            label: {
                text: locations_booked[i][6],
                color: '#666666',
            }
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                if (marker.getAnimation() !== null) {
                    marker.setAnimation(null);
                } else {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                }
                map.setZoom(17);
                myLatLng = new google.maps.LatLng(locations_booked[i][1], locations_booked[i][2]);
                marker.setPosition(myLatLng);
                map.panTo(myLatLng);
            }
        })(marker, i));
        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
            return function () {
                infowindow.setContent("<div><img src='" + locations_booked[i][4] + "' width='200px' height='130px'></div><h5>" + locations_booked[i][0] + "</h5><span>" + locations_booked[i][3] + "</span>");
                infowindow.open(map, marker);
            }
        })(marker, i));
        google.maps.event.addListener(marker, 'mouseout', (function (marker, i) {
            return function () {
                marker.setAnimation(null);
                infowindow.close();
            }
        })(marker, i));
    }
}

