<p align="center"><a href="https://codeigniter.com" target="_blank"><img src="https://www.nesabamedia.com/wp-content/uploads/2019/09/CodeIgniter.png" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

Sistem Geografis Tata Letak Billboard di kota Pekalongan, berbasis website dengan menggunakan framework CodeIgniter dan database MySQL

## Sistem Geografis Tata Letak Billboard

Untuk demo program silahkan [klik disini](https://rickstars.xyz/gis-billboard).
